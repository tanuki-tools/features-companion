# features-companion

## Requirements

- Install the ViolentMonkey extension
  - from Chrome Store: [https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag?hl=en](https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag?hl=en)
  - (right now, not tested) from Mozilla add-ons page: [https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/)

## Install the "features-companion" script

- Add script (Click on the **+** button)
- Choose: "Install from url" and add this url: [https://gitlab.com/tanuki-tools/features-companion/raw/master/index.js](https://gitlab.com/tanuki-tools/features-companion/raw/master/index.js)
- Once the script loaded, click on "Confirm installation"
- The script is loaded

### Add the GitLab instance domain

To activate the "features-companion" script on a specific domain of a GitLab instance, you need to add script variables:
- Select the **Values** panel
- Add a script value with:
  - `hosts` as key
  - an array of strings like that `["gitlab-faas.eu.ngrok.io", "gitlab.com"]` as value
- That's all

## TODO

- Load the rules from a remote jsonfile (or local?)

