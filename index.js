// ==UserScript==
// @name featuresCompanion
// @namespace botsGarden
// @version 1.1.1
// @match *://*/*
// @grant GM_getValue
// ==/UserScript==
// 

// WIP 🚧
function getRules() {
  let rules = [
    {
      name:"Epics",
      words: ["epics"],
      edition: "Ultimate"
    },
    {
      name:"Epics roadmap",
      words: ["groups", "roadmap"],
      edition: "Ultimate"
    },    
    {
      name:"Group issues",
      words: ["groups", "issues"],
      edition: "Premium"
    },
    {
      name:"Group milestones",
      words: ["groups", "milestones"],
      edition: "Premium"
    },
    {
      name:"Group labels",
      words: ["groups", "labels"],
      edition: "Premium"
    },
    {
      name:"Group boards",
      words: ["groups", "boards"],
      edition: "Premium"
    }
  ]  
  return rules
}

function getLocation() {
  let location = {
    hash: window.location.hash,
    host: window.location.host,
    hostname: window.location.hostname,
    href: window.location.href,
    origin: window.location.origin,
    pathname: window.location.pathname,
    port: window.location.port,
    protocol: window.location.protocol
  } 
  console.log(location);
  return location
}

function addItemToNavBar() {
  let navbar = document.querySelector("ul.navbar-sub-nav")
  let node = document.createElement("li")
  node.classList.add("d-none")
  node.classList.add("d-lg-block")
  node.classList.add("d-xl-block")
  let link = document.createElement("a")
  link.classList.add("dashboard-shortcuts-snippets")
  link.innerHTML = "..."
  node.appendChild(link)
  navbar.appendChild(node)
  return link
}

/**
 * Add script values
 * key: hosts
 * values: an array of hosts like that: ["gitlab-faas.eu.ngrok.io", "gitlab.com"] 
 */
function getHosts() {
  return GM_getValue("hosts", ["gitlab.com"]) 
}

(function(){
  let hosts = getHosts()
  let location = getLocation()
  
  if(hosts.includes(location.host)) {

    let link = addItemToNavBar()
    
    link.innerHTML = "🦊"
    
    // check all rules
    getRules().forEach(item => {
      // check rules
      var included = 0
      item.words.forEach(word => {
        if(location.pathname.includes(word)) {included+=1}
      })
      if(included>0 && item.words.length == included) {
        link.innerHTML = `👋 > ${item.edition}: ${item.name}`
      } 
    })

    // detect click on nodes
    let otherLink = addItemToNavBar()

    document.body.addEventListener('click', (event) => {
      console.log(event)
      let firstChild = event.srcElement.firstChild
      otherLink.innerHTML = `🐭 > ${firstChild.nodeValue || firstChild.textContent || firstChild.data}`
    }, true);

  }
  
})()

  

